
// Project Name
// Your Name

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void Display(string *pInputs, ostream &out = cout)
{
	out <<
		"One day my " << pInputs[0] << " friend and I decided to go to the " << pInputs[1] << " game in " << pInputs[2] << ".\n"
		<< "We really wanted to see " << pInputs[3] << " play.\n"
		"So we " << pInputs[4] << " in the " << pInputs[5] << "and headed down to the " << pInputs[6] << " and bought some " << pInputs[7] << ".\n"
		"We watched the game and it was " << pInputs[8] << ".\n"
		"We ate some " << pInputs[9] << " and drank some " << pInputs[10] << ".\n"
		"We had a " << pInputs[11] << " time, and can\'t wait to go again.\n";

}

int main()
{
	string filepath = "C:\\temp\\test.txt";

	const int NUM_LIBS = 12;
	string inputs[NUM_LIBS];

	string questions[NUM_LIBS] = 
	{
		"adjective", "noun", "person", 
		"place", "verb", "noun", "city", 
		"food", "day",  "person", "baseball team", "noun"
	};



	for (int i = 0; i < NUM_LIBS; i++)
	{
	cout << "Enter a " << questions[i] <<": ";
	getline(cin, inputs[i]);
	}

	cout << "\n";

	Display(inputs);


	char choice = 'n';
	cout << "Do you want to save to text file? (y/n)";
	

	cin >> choice;

	if (choice == 'y')
	{
		ofstream fout(filepath);

		if (fout)
		{
			Display(inputs, fout);
			fout.close();

		}
	}
	

	_getch();
	return 0;
}
